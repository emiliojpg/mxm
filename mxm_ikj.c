#include <stdio.h>
#include "stuff.h"

#ifndef N
#define N 1024
#endif

basetype matrix_a[N][N];
basetype matrix_b[N][N];
basetype result_matrix[N][N];

int main(int argc, char **argv) {
  resnfo start, end;
  timenfo time;

  populating_arrays(&matrix_a[0][0], &matrix_b[0][0], &result_matrix[0][0], N*N);

  timestamp(&start);

#pragma omp parallel for
  for (int i = 0; i < N; i++) {
      for (int k = 0; k < N; k++) {
        for (int j = 0; j < N; j++) {
        result_matrix[i][j] += matrix_a[i][k]*matrix_b[k][j];
      }
    }
  }

  timestamp(&end);

  get_walltime(start, end, &time);
  printtime(time);
  printf(" -> Matrix (%dx%d %s) product ikj: %s\n", N, N, labelelem, argv[0]);

  basetype check = check_result(&result_matrix[0][0], N*N);
  printf("%29s", "Check: "); printdata(check); printf("\n");

  char *md5check = md5((unsigned char *) &result_matrix[0][0], N*N*sizeof(basetype));
  printf("%28s %s\n\n", "MD5:", md5check);
  free(md5check);
}
