#include "matrix_add.h"

void matrix_add (const basetype *const a, const basetype *const b, basetype *const c, unsigned int nelem)
{
  int unsigned i;

  for (i = 0; i < nelem; ++i)
    c[i] = a[i] + b[i];
}
