#include <stdio.h>
#include "stuff.h"

#ifndef N
#define N 1024
#endif

basetype matrix[N][N];

int main(int argc, char **argv) {
  resnfo start, end;
  timenfo time;

  populating_array(&matrix[0][0], N*N);

  timestamp(&start);

#pragma omp parallel for
  for (int i = 0; i < N; i++) {
    for (int j = 1; j < N; j++) {
      matrix[i][j]=matrix[i][j-1];
    }
  }

  timestamp(&end);

  get_walltime(start, end, &time);
  printtime(time);
  printf(" -> Matrix (%dx%d %s) loop carried dependency: %s\n", N, N, labelelem, argv[0]);

  basetype check = check_result(&matrix[0][0], N*N);
  printf("%29s", "Check: "); printdata(check); printf("\n");

  char *md5check = md5((unsigned char *) &matrix[0][0], N*N*sizeof(basetype));
  printf("%28s %s\n\n", "MD5:", md5check);
  free(md5check);
}
