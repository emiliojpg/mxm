TARGETS = mxm mxm_ikj mxm_ijk_t red loopdep addingmatrices

O = -O3 -march=native
CFLAGS = $(O) -fopenmp -fopt-info-optall-optimized -Wall
LDFLAGS = -fopenmp

all: $(TARGETS)

mxm: mxm.o stuff.o

mxm_ikj: mxm_ikj.o stuff.o

mxm_ijk_t: mxm_ijk_t.o stuff.o

red: red.o stuff.o

loopdep: loopdep.o stuff.o

addingmatrices: addingmatrices.o matrix_add.o stuff.o

clean:
	rm -f *.o *.s *~

cleanall: clean
	rm -f $(TARGETS)
