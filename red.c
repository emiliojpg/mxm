#include <stdio.h>
#include "stuff.h"

#ifndef N
#define N 1024
#endif

basetype matrix[N][N];

int main(int argc, char **argv) {
  resnfo start, end;
  timenfo time;

  populating_array(&matrix[0][0], N*N);

  timestamp(&start);

  double result = 0, tmp;

#pragma omp parallel for
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; j++) {
      tmp = matrix[i][j];
      result = result + tmp * tmp;
    }
  }

  timestamp(&end);

  get_walltime(start, end, &time);
  printtime(time);
  printf(" -> Matrix (%dx%d %s) reduction: %s\n", N, N, labelelem, argv[0]);

  printf("%29s", "Check: "); printdata(result); printf("\n");

  char *md5check = md5((unsigned char *) &matrix[0][0], N*N*sizeof(basetype));
  printf("%28s %s\n\n", "MD5:", md5check);
  free(md5check);
}
